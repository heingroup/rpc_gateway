import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

import logging
import click
import signal
from rpc_gateway.gateway import Gateway

signal.signal(signal.SIGINT, signal.SIG_DFL)  # magic line to fix ctrl-c on Windows when using asyncio
DEFAULT_HOST = '0.0.0.0'
DEFAULT_PORT = 8888
DEFAULT_HTTP_PORT = 8887
DEFAULT_KEY = os.environ.get('RPC_GATEWAY_AUTH_KEY', 'DEFAULT_KEY')
LOG_LEVELS = (logging.WARN, logging.INFO, logging.DEBUG)


@click.command()
@click.option('--host', '-h', default=DEFAULT_HOST, help='Hostname for the gateway server')
@click.option('--port', '-p', default=DEFAULT_PORT, help='Port for the gateway server')
@click.option('--http-port', '-P', default=DEFAULT_HTTP_PORT, help='Port for the gateway HTTP server')
@click.option('--auth-key', '-A', default=DEFAULT_KEY, help='Authentication key')
@click.option('--verbose', '-v', count=True, default=2)
def run(host, port, http_port, verbose, auth_key):
    verbose = max(min(len(LOG_LEVELS), verbose), 0)
    logging.basicConfig(level=LOG_LEVELS[verbose])

    gateway = Gateway(host, port, http_port=http_port, auth_key=auth_key)
    gateway.start()


if __name__ == '__main__':
    run()
