import logging
import time
from rpc_gateway.client import Client
from north_devices.power_supplies.keithley import KeithleyPS, KeithleyPSChannel

logging.basicConfig(level=logging.WARNING)

client = Client('ws://20.36.16.192')

channel: KeithleyPSChannel = client.get_instance('ps.channels[0]')

while True:
    print(channel.measured_current)
    # time.sleep(0.5)
