import logging
from threading import Thread
from queue import Queue
from rpc_gateway.server import Server

logging.basicConfig(level=logging.INFO)


class TestClass:
    foo = 'bar'

    def method(self, *args):
        return args


def queue_reader():
    while True:
        value = queue.get()
        print('value: ', value)
        response_queue.put(value + 'bar')


queue = Queue()
response_queue = Queue()
queue_thread = Thread(target=queue_reader)
queue_thread.start()

server = Server()
server.register('test', TestClass())
server.register('queue', queue)
server.register('response_queue', response_queue)

server.start()
