#!/usr/bin/env bash

echo "Please enter an email to use for the letsencrypt certificate:"
read email

echo "Generating temporary keys..."
mkdir -p config/certbot/conf/{live,www}/rpc.heinlab.com
docker-compose -f docker-compose-prod.yml run --rm --entrypoint "openssl req -x509 -nodes -newkey rsa:1024 -days 1 -keyout /etc/letsencrypt/live/rpc.heinlab.com/privkey.pem -out /etc/letsencrypt/live/rpc.heinlab.com/fullchain.pem -subj '/CN=localhost'" certbot

echo "Running with temporary keys..."
docker-compose -f docker-compose-prod.yml up -d

docker-compose -f docker-compose-prod.yml run --rm --entrypoint "\
  rm -Rf /etc/letsencrypt/live/rpc.heinlab.com && \
  rm -Rf /etc/letsencrypt/archive/rpc.heinlab.com && \
  rm -Rf /etc/letsencrypt/renewal/rpc.heinlab.com*" certbot

echo "Requesting letsencrypt certificate..."
docker-compose -f docker-compose-prod.yml run --rm --entrypoint "certbot certonly --webroot -w /var/www/certbot --email $email --no-eff-email -d rpc.heinlab.com --agree-tos" certbot

echo "Stopping..."
docker-compose -f docker-compose-prod.yml down