#!/usr/bin/env bash

echo "Generating config/auth_key.txt..."
auth_key=$(date | md5sum | awk '{print $1}')
echo "RPC_GATEWAY_AUTH_KEY=${auth_key}" >> .env