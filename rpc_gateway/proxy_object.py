from typing import Any
from rpc_gateway import errors


def build(client, instance_name: str, attribute_path: str, read_only: bool = False) -> Any:
    class _Proxy:
        def __getattr__(self, key):
            return client.get(instance_name, client.join_attribute_paths(attribute_path, key))

        def __setattr__(self, key, value):
            if read_only:
                raise errors.InstanceReadOnly(f'Cannot set attribute {key} on read-only instance')

            return client.set(instance_name, client.join_attribute_paths(attribute_path, key), value)

        def __dir__(self):
            return client.call(instance_name, client.join_attribute_paths(attribute_path, '__dir__'))

        def __repr__(self):
            return client.call(instance_name, client.join_attribute_paths(attribute_path, '__repr__'))

        def __str__(self):
            return client.call(instance_name, client.join_attribute_paths(attribute_path, '__str__'))

        def __bool__(self):
            return client.call(instance_name, client.join_attribute_paths(attribute_path, '__bool__'))

        def __bytes__(self):
            return client.call(instance_name, client.join_attribute_paths(attribute_path, '__bytes__'))

        def __int__(self):
            return client.call(instance_name, client.join_attribute_paths(attribute_path, '__int__'))

        def __complex__(self):
            return client.call(instance_name, client.join_attribute_paths(attribute_path, '__complex__'))

        def __float__(self):
            return client.call(instance_name, client.join_attribute_paths(attribute_path, '__float__'))

        def __hash__(self):
            return client.call(instance_name, client.join_attribute_paths(attribute_path, '__hash__'))

        # container types

        def __len__(self, *args):
            return client.call(instance_name, client.join_attribute_paths(attribute_path, '__len__'), args)

        def __getitem__(self, *args):
            return client.call(instance_name, client.join_attribute_paths(attribute_path, '__getitem__'), args)

        def __setitem__(self, *args):
            return client.call(instance_name, client.join_attribute_paths(attribute_path, '__setitem__'), args)

        def __delitem__(self, *args):
            return client.call(instance_name, client.join_attribute_paths(attribute_path, '__delitem__'), args)

        def __missing__(self, *args):
            return client.call(instance_name, client.join_attribute_paths(attribute_path, '__missing__'), args)

        def __contains__(self, *args):
            return client.call(instance_name, client.join_attribute_paths(attribute_path, '__contains__'), args)

        # numberic types

        def __add__(self, *args):
            return client.call(instance_name, client.join_attribute_paths(attribute_path, '__add__'), args)

        def __sub__(self, *args):
            return client.call(instance_name, client.join_attribute_paths(attribute_path, '__sub__'), args)

        def __mul__(self, *args):
            return client.call(instance_name, client.join_attribute_paths(attribute_path, '__mul__'), args)

        def __matmul__(self, *args):
            return client.call(instance_name, client.join_attribute_paths(attribute_path, '__matmul__'), args)

        def __truediv__(self, *args):
            return client.call(instance_name, client.join_attribute_paths(attribute_path, '__truediv__'), args)

        def __floordiv__(self, *args):
            return client.call(instance_name, client.join_attribute_paths(attribute_path, '__floordiv__'), args)

        def __mod__(self, *args):
            return client.call(instance_name, client.join_attribute_paths(attribute_path, '__mod__'), args)

        def __divmod__(self, *args):
            return client.call(instance_name, client.join_attribute_paths(attribute_path, '__divmod__'), args)

        def __pow__(self, *args):
            return client.call(instance_name, client.join_attribute_paths(attribute_path, '__pow__'), args)

        def __lshift__(self, *args):
            return client.call(instance_name, client.join_attribute_paths(attribute_path, '__lshift__'), args)

        def __rshift__(self, *args):
            return client.call(instance_name, client.join_attribute_paths(attribute_path, '__rshift__'), args)

        def __and__(self, *args):
            return client.call(instance_name, client.join_attribute_paths(attribute_path, '__and__'), args)

        def __xor__(self, *args):
            return client.call(instance_name, client.join_attribute_paths(attribute_path, '__xor__'), args)

        def __or__(self, *args):
            return client.call(instance_name, client.join_attribute_paths(attribute_path, '__or__'), args)

        def __neg__(self, *args):
            return client.call(instance_name, client.join_attribute_paths(attribute_path, '__neg__'), args)

        def __pos__(self, *args):
            return client.call(instance_name, client.join_attribute_paths(attribute_path, '__pos__'), args)

        def __abs__(self, *args):
            return client.call(instance_name, client.join_attribute_paths(attribute_path, '__abs__'), args)

        def __invert__(self, *args):
            return client.call(instance_name, client.join_attribute_paths(attribute_path, '__invert__'), args)

        def __index__(self, *args):
            return client.call(instance_name, client.join_attribute_paths(attribute_path, '__index__'), args)

        def __round__(self, *args):
            return client.call(instance_name, client.join_attribute_paths(attribute_path, '__round__'), args)

        def __trunc__(self, *args):
            return client.call(instance_name, client.join_attribute_paths(attribute_path, '__trunc__'), args)

        def __floor__(self, *args):
            return client.call(instance_name, client.join_attribute_paths(attribute_path, '__floor__'), args)

        def __ceil__(self, *args):
            return client.call(instance_name, client.join_attribute_paths(attribute_path, '__ceil__'), args)

    return _Proxy()