FROM python:3.8
WORKDIR /usr/rpc_gateway
EXPOSE 8888 8887
COPY requirements.txt .
RUN apt-get update
RUN apt-get install -y libsdl1.2-dev libsdl-image1.2-dev libsdl-ttf2.0-dev libsdl-mixer1.2-dev libportmidi-dev
RUN pip install -r requirements.txt
COPY . .
CMD ["python", "./scripts/run_gateway.py", "-v"]