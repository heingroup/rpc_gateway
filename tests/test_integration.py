from unittest import TestCase
import time
import logging
import asyncio
from rpc_gateway import server, client, gateway, errors, messages_pb2

# logging.basicConfig(level=logging.INFO)


class DataClass:
    """
    A test class
    """
    def __init__(self, data=None):
        self.data = data or {}
        self.file = open('../requirements.txt')
        self.int_value = 20000
        self.float_value = 1.0
        self.bool_value = True

    @property
    def int_value_prop(self):
        return self.int_value

    @int_value_prop.setter
    def int_value_prop(self, value):
        self.int_value = value

    def get_data(self, key):
        """ get data with the given key """
        keys = '.'.split(key)
        data = self.data

        for i in range(len(keys)):
            data = data[keys[i]]

            if i == len(keys) - 1:
                return data


class ServerClass:
    """
    A test server class
    """
    class_data = DataClass({ 'class': 'ServerClass' })

    def __init__(self):
        self.instance_data = DataClass({'instance_of': 'ServerClass'})

    def get_data(self, key):
        """ get class_data data """
        return self.class_data.get_data(key)

    def test(self, bar):
        print('TEST!', bar)
        return bar


class TestIntegration(TestCase):
    gateway: gateway.Gateway
    server: server.Server
    client: client.Client

    @classmethod
    def setUpClass(cls) -> None:
        cls.event_loop = asyncio.get_event_loop()
        cls.gateway = gateway.Gateway(event_loop=cls.event_loop)
        cls.gateway.start(wait=False)

    @classmethod
    def tearDownClass(cls) -> None:
        cls.gateway.stop()

    def setUp(self) -> None:
        self.server = server.Server()
        self.client = client.Client()

    def test_basic(self):
        self.server.register('instance', ServerClass())
        self.server.start(wait=False)
        instance: ServerClass = self.client.get_instance('instance')

        self.assertEqual(instance.class_data.data['class'], 'ServerClass')
        print(instance)

    def test_lock(self):
        self.server.register('instance', ServerClass())
        self.server.start(wait=False)

        with self.assertRaises(errors.InstanceNotFoundError):
            self.client.get_instance('not_found')

        self.assertEqual(self.client.instance_exists('instance'), True)
        self.assertEqual(self.client.instance_available('instance'), True)
        self.assertEqual(self.client.instance_locked('instance'), False)

        instance: ServerClass = self.client.get_instance('instance')

        self.assertEqual(self.client.instance_available('instance'), False)
        self.assertEqual(self.client.instance_locked('instance'), True)

        with self.assertRaises(errors.InstanceLockedError):
            self.client.get_instance('instance')

        self.client.unlock('instance')
        self.assertEqual(self.client.instance_available('instance'), True)
        self.assertEqual(self.client.instance_locked('instance'), False)

        instance2: ServerClass = self.client.get_instance('instance')

        self.assertEqual(instance2.class_data.data['class'], 'ServerClass')

    def test_metadata(self):
        self.server.register('instance', ServerClass())
        self.server.start(wait=False)

        print(self.client.metadata('instance'))
        print()

    def test_http(self):
        self.server.register('instance', ServerClass())
        self.server.start(wait=True)

        print(self.server)

    def test_list(self):
        self.server.register('instance', ServerClass())
        self.server.register('instance2', ServerClass())
        self.server.register('test', ServerClass(), group='test')
        self.server.register('test2', ServerClass(), group='test')
        self.server.start(wait=False)
        print(self.client.list())
        print(self.client.list('ServerClass'))
        print(self.client.list('test'))

    def test_deregister(self):
        self.server.register('instance', ServerClass())
        self.server.start(wait=False)

        self.assertDictEqual({'ServerClass': ['instance']}, self.client.list())
        self.assertIn('instance', self.gateway.instances)

        self.server.deregister('instance')
        self.assertNotIn('instance', self.gateway.instances)

    def test_register(self):
        with self.assertRaises(errors.InvalidInstanceError):
            self.server.register(None, None)

        with self.assertRaises(errors.InvalidInstanceError):
            self.server.register('', None)

        self.server.register('test', 'test')
        self.server.start(wait=False)
        time.sleep(1)

        with self.assertRaises(errors.InvalidMessageError):
            self.server.websocket_connection.send_request_sync(messages_pb2.Method.REGISTER, [(None, None)])

        self.assertIn('test', self.gateway.instances)

    def test_disconnect(self):
        self.server.register('instance', ServerClass())
        self.server.start(wait=False)

        time.sleep(1)
        self.assertEqual(len(self.gateway.websocket_connections), 1)

        self.server.stop()

        time.sleep(2)

        self.assertEqual(len(self.gateway.websocket_connections), 0)
        self.server.register('instance', ServerClass())

    def test_proxy_casting(self):
        self.server.register('instance', ServerClass())
        self.server.start(wait=False)
        instance: ServerClass = self.client.get_instance('instance')

        self.assertEqual(int(instance.class_data.int_value), 20000)
        self.assertEqual(int(instance.class_data.int_value_prop), 20000)
        old = int(instance.class_data.int_value_prop)
        instance.class_data.int_value_prop = 2
        self.assertEqual(int(instance.class_data.int_value_prop), 2)
        instance.class_data.int_value_prop = old
        self.assertEqual(int(instance.class_data.int_value_prop), 20000)
