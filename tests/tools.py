from typing import Optional, List
from unittest import TestCase
from inspect import iscoroutinefunction
import websockets
import asyncio
import pickle
from rpc_gateway import messages


def async_test(event_loop):
    def wrapper(coro):
        def _wrapper(self, *args, **kwargs):
            return event_loop.run_until_complete(coro(self, *args, **kwargs))

        return _wrapper
    return wrapper


async def echo_request_handler(req):
    return messages.Response(id=req.id, data={'method': req.method, 'data': req.data})


class EchoServer:
    def __init__(self, host: str = 'localhost', port: int = 8888):
        self.host = host
        self.port = port
        self.websocket: Optional[websockets.WebSocketServerProtocol] = None
        self.messages = asyncio.Queue()
        self.echo = True
        self.message_handler = None

    def start(self, loop: asyncio.AbstractEventLoop):
        loop.run_until_complete(self._start())

    async def _start(self):
        self.websocket = await websockets.serve(self._on_connection, self.host, self.port)

    def stop(self, loop: asyncio.AbstractEventLoop):
        loop.run_until_complete(self._stop())

    async def _stop(self):
        self.websocket.close()
        await self.websocket.wait_closed()

    def clear_messages(self):
        self.messages = asyncio.Queue()

    async def _on_connection(self, connection, path):
        try:
            while True:
                data = await connection.recv()
                message = pickle.loads(data)
                await self.messages.put(message)

                if self.message_handler is not None:
                    response = (await self.message_handler(message)).to_pickle()
                else:
                    response = data

                if self.echo:
                    await connection.send(response)
        except websockets.ConnectionClosed:
            pass
