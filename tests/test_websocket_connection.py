import asyncio
import websockets
from unittest import TestCase
from rpc_gateway import messages
from rpc_gateway.websocket_connection import WebsocketConnection
from tests.tools import EchoServer, async_test, echo_request_handler

event_loop = asyncio.new_event_loop()


class TestWebsocketConnection(TestCase):
    connection: WebsocketConnection = None
    echo_server: EchoServer = None

    @classmethod
    @async_test(event_loop)
    async def setUpClass(cls) -> None:
        cls.echo_server = EchoServer()
        await cls.echo_server._start()

    @classmethod
    def tearDownClass(cls) -> None:
        cls.echo_server.stop(event_loop)
        event_loop.close()

    @async_test(event_loop)
    async def setUp(self) -> None:
        self.echo_server.echo = True
        self.echo_server.message_handler = None
        self.connection = WebsocketConnection(await websockets.connect('ws://localhost:8888'))
        await self.connection.start(wait=False)

    @async_test(event_loop)
    async def tearDown(self) -> None:
        self.echo_server.clear_messages()
        await self.connection.stop()

    @async_test(event_loop)
    async def test_send_message(self):
        self.echo_server.echo = False
        message = messages.Message(id=1)
        await self.connection.send_message(message)
        response = await self.echo_server.messages._get()
        self.assertIsInstance(response, messages.Message)
        self.assertDictEqual(message.to_dict(), response.to_dict())

    @async_test(event_loop)
    async def test_request(self):
        self.echo_server.message_handler = echo_request_handler
        response = await self.connection.send_request(messages.Method.GET, 'test')
        self.assertIsInstance(response, messages.Response)
        self.assertDictEqual(response.data, {'method': messages.Method.GET, 'data': 'test'})
