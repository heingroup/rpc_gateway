from unittest import TestCase
from rpc_gateway import registry


class TestClass:
    foo = 'bar'

    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs


class TestClass2:
    foo = 'baz'

    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs


class TestRegistry(TestCase):
    def setUp(self) -> None:
        registry.Registry.reset()

    def test_get_default_registry(self):
        self.assertIsNone(registry.Registry._default_registry)
        reg = registry.Registry.default_registry()
        reg2 = registry.default_registry()
        self.assertIs(reg, reg2, registry.Registry._default_registry)

    def test_register(self):
        reg = registry.Registry()
        reg.register(TestClass, 'test')
        self.assertIn('test', reg.factories)
        self.assertIn(TestClass, reg.factory_types)
        registry.register(TestClass, 'test2')
        self.assertIn('test2', registry.default_registry().factories)
        self.assertIn(TestClass, registry.default_registry().factory_types)

    def test_get_by_name(self):
        registry.register(TestClass, 'test')
        test = registry.default_registry().get_by_name('test')
        self.assertIsInstance(test, TestClass)

        with self.assertRaises(registry.RegistryFactoryNotFoundError):
            registry.default_registry().get_by_name('foobar')

    def test_get_by_type(self):
        registry.register(TestClass)
        test = registry.default_registry().get_by_type(TestClass)
        self.assertIsInstance(test, TestClass)

        with self.assertRaises(registry.RegistryFactoryNotFoundError):
            registry.default_registry().get_by_type(TestClass2)

    def test_get(self):
        registry.register(TestClass)
        registry.register(TestClass2, 'test')

        test = registry.get(TestClass)
        test2 = registry.get('test')

        self.assertIsInstance(test, TestClass)
        self.assertIsInstance(test2, TestClass2)

        with self.assertRaises(registry.RegistryFactoryNotFoundError):
            registry.get('foobar')

        with self.assertRaises(registry.RegistryFactoryNotFoundError):
            registry.get(TestRegistry)

    def test_configure_dict(self):
        registry.configure({
            'test': ['arg1', 'arg2'],
            'TestClass2': {
                '$args': ['arg1'],
                'foo': 'bar'
            }
        })

        registry.register(TestClass, 'test')
        registry.register(TestClass2)

        test: TestClass = registry.get('test')
        self.assertIsInstance(test, TestClass)
        self.assertListEqual(list(test.args), ['arg1', 'arg2'])

        test2: TestClass2 = registry.get(TestClass2)
        self.assertIsInstance(test2, TestClass2)
        self.assertListEqual(list(test2.args), ['arg1'])
        self.assertDictEqual(test2.kwargs, {'foo': 'bar'})

    def test_configure_json(self):
        registry.configure('./files/config.json')

        registry.register(TestClass, 'test')
        registry.register(TestClass2)

        test: TestClass = registry.get('test')
        self.assertIsInstance(test, TestClass)
        self.assertListEqual(list(test.args), ['arg1', 'arg2'])

        registry.default_registry().reset_object('test')
        test: TestClass = registry.get('test', 'arg3', foo='bar')
        self.assertListEqual(list(test.args), ['arg1', 'arg2', 'arg3'])

        test2: TestClass2 = registry.get(TestClass2)
        self.assertIsInstance(test2, TestClass2)
        self.assertListEqual(list(test2.args), ['arg1'])
        self.assertDictEqual(test2.kwargs, {'foo': 'bar'})

    def test_configure_yaml(self):
        registry.configure('./files/config.yaml')

        registry.register(TestClass, 'test')
        registry.register(TestClass2)

        test: TestClass = registry.get('test')
        self.assertIsInstance(test, TestClass)
        self.assertListEqual(list(test.args), ['arg1', 'arg2'])

        registry.default_registry().reset_object('test')
        test: TestClass = registry.get('test', 'arg3', foo='bar')
        self.assertListEqual(list(test.args), ['arg1', 'arg2', 'arg3'])

        test2: TestClass2 = registry.get(TestClass2)
        self.assertIsInstance(test2, TestClass2)
        self.assertListEqual(list(test2.args), ['arg1'])
        self.assertDictEqual(test2.kwargs, {'foo': 'bar'})

    def test_type_key(self):
        registry.configure('./files/config.yaml')

        registry.register(TestClass2)

        test = registry.get(TestClass2)
        foo = registry.get('foo')

        self.assertIsInstance(test, TestClass2)
        self.assertIsInstance(foo, TestClass2)
        self.assertIsNot(test, foo)
        self.assertDictEqual(test.kwargs, {'foo': 'bar'})
        self.assertDictEqual(foo.kwargs, {'foo': 'baz'})

    def test_multiple_config(self):
        registry.configure('./files/parent_config.yaml')
        registry.configure('./files/child_config.yaml')

        registry.register(TestClass)

        parent = registry.get(TestClass)
        child = registry.get('test')

        self.assertIsInstance(parent, TestClass)
        self.assertIsInstance(child, TestClass)
        self.assertIsNot(parent, child)
        self.assertDictEqual(parent.kwargs, {'root': 'TestClass', 'parent': ''})
        self.assertDictEqual(child.kwargs, {'root': 'TestClass', 'parent': 'TestClass', 'foo': 'baz'})

    def test_config_object_injection(self):
        registry.configure('./files/injection.yaml')

        registry.register(TestClass)
        registry.register(TestClass2)

        main_instance: TestClass2 = registry.get('main_instance')

        self.assertIsInstance(main_instance, TestClass2)
        self.assertIsInstance(main_instance.kwargs['injected'], TestClass)
        self.assertDictEqual(main_instance.kwargs['injected'].kwargs, {'info': 'injected'})

    def test_config_value_injection(self):
        registry.configure('./files/config_value.yaml')
        registry.register(TestClass)

        test: TestClass = registry.get(TestClass)
        self.assertIsInstance(test, TestClass)
        self.assertDictEqual(test.kwargs, {'foo': 'bar', 'baz': 'foobar'})
